"uses strict";
const express = require("express");
const bodyParser = require("body-parser");
const mongodb = require("mongodb");
const request = require('request');
const app = express();
const restAPI = express.Router();
const server_port = 3000;
//const content = require('./content.js');
//const html_content1  = content.content1;

const conf = {encoding: "utf-8"};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({"extended": true}));
app.set('view engine', 'jade');

var MongoClient = mongodb.MongoClient;
var mongo_url = 'mongodb://localhost:27017/notebook';
var host = 'http://localhost'

let counter = 0;

//============================views=======================================

app.get("/", function(req, res) {
  res.render('start', { title: 'Домашнее заданее MongoDB-API:стартовая страница', message: 'Добрый день!' })
});

app.get("/contacts", function(req, res) {
  request({
    method: 'GET',
    url: host + ':' + server_port + '/api/v1/contacts/',
    json: true,
    headers: {
    'User-Agent': 'request'
    }
   }, (err2, res2, body2) => {
    console.log(res2.statusCode);
    console.log(res2.body);
    res.render('contacts', { contacts: res2.body, message: 'Список всех контактов!' });

  });
  
});

app.get("/add_contact", function(req, res) {
  res.render('add_contact', { title: 'Добавить контакт', message: 'Добавить контакт!' })
});

app.get("/find", function(req, res) {
  res.render('find', { title: 'Поиск ', message: 'Поиск контакта!' })
});

app.get("/delete", function(req, res) {
  res.render('delete', { title: 'Удаление контакта', message: 'Удалить контакт!' })
});



//============================views=======================================

//==========================restAPI=======================================
restAPI.get("/", function(req,res){
  //res.render('start', { title: 'Start page', message: 'Hello there!' })
});

/*

Описание API
вывод ы JSON формате

host:port/api/v1/contacts

    GET - чтение контактов
          выдает список контактов в массиве JSON
    POST - создать нового пользователя

host:port/api/v1/contacts/:id
    GET - найти пользователя
    PUT - обновить данные о пользователе
    DELETE - удалить пользователя

host:port/api/v1/contacts?name=Anya
список контактов с именем Аня

коды ответов HTTP
200 - OK
400 - Bad Request (некорректный запрос)
500 - Internal Srever Error
*/

restAPI.get("/", function(req, res) {
//Начальная страница
//Вывести html content


})



restAPI.get("/contacts/", function(req, res) { 

    MongoClient.connect(mongo_url, function(err, db) {
      if(err) {
        console.log("Невозможно подключиться к базе данных");
        res.status(500);
      } else {    //else01    
        console.log('Connection is established for ', mongo_url); 
        var collection = db.collection ('contacts');
        collection.find().toArray(function(err,result) {
          console.log('содержание коллекции contacts ',result);
          res.status(200).json(result);
          res.end();
        });
      } //ends else01
      db.close();
    })
		
});	

restAPI.post("/contacts/", function(req, res) { 
  let post_request = req.body;
  console.log('Поступил POST запрос');
  let post_name = post_request.name;
  console.log("post_request.name", post_request.name);
  let post_second_name = post_request.second_name;
  console.log("post_request.second_name", post_request.second_name);
  let post_phone_number = post_request.phone_number;
  console.log("post_request.phone_number", post_request.phone_number);
  let new_contact = {
  	"name"   : post_name,
  	"second_name" : post_second_name,
  	"phone_number": post_phone_number 
  };

  MongoClient.connect(mongo_url, function(err, db) {
      if(err) {
        console.log("Невозможно подключиться к базе данных");
        res.status(500);
      } else {    //else    
        console.log('Connection is established for ', mongo_url); 
        var collection = db.collection ('contacts');
        collection.insert(new_contact, function(err, result) {
          if(err){
            console.log("Ошибка при добавлении данных в БД", err);
            res.status(500);
          } else {
            console.log('Запись добавлена в БД', result);
            res.status(200).json(result);
            res.end();
          };

        });

      } //ends else
      db.close();
  });

});

restAPI.get("/contacts", function(req, res) { 
//найти пользователя contacts?name=Vasya
    var query=req.query;
    MongoClient.connect(mongo_url, function(err, db) {
      if(err) {
        console.log("Невозможно подключиться к базе данных");
        res.status(500);
      } else {    //else    
        console.log('Connection is established for ', mongo_url); 
        var collection = db.collection ('contacts');
        if(query.name){
          collection.find({"name":query.name}, function(err, result) {
            if(err){
              console.log("Ошибка поиска в БД", err);
              res.status(500);
            } else {
              console.log('Результат поиска', result);
              res.status(200).json(result);
              res.end();
            };
          });       
        } else if(query.second_name){
          collection.find({"second_name":query.second_name}, function(err, result) {
            if(err){
              console.log("Ошибка поиска в БД", err);
              res.status(500);
            } else {
              console.log('Результат поиска', result);
              res.status(200).json(result);
              res.end();
            };
          });       
        } else if(query.phone_number){
          collection.find({"name":query.phone_number}, function(err, result) {
            if(err){
              console.log("Ошибка поиска в БД", err);
              res.status(500);
            } else {
              console.log('Результат поиска', result);
              res.status(200).json(result);
              res.end();
            };
          });       

        } else {
          res.status(404);
          res.end();
        }
        
      } //ends else
      db.close();
    });

});

restAPI.put("/users/:id", function(req, res) { 
//put идемпотентен
//обновить данные пользователя по id
  console.log("req.body", req.body[0]);
  console.log("req.body.score", req.body[0].score);
  let put_request = req.body;
  

  res.status(200).json(contacts);
  res.end();

});

restAPI.delete("/contacts/:id", function(req, res) { 
//удалить пользователя по id
  	console.log(req.params.id);
  MongoClient.connect(mongo_url, function(err, db) {
      if(err) {
        console.log("Невозможно подключиться к базе данных");
        res.status(500);
      } else {    //else    
        console.log('Connection is established for ', mongo_url); 
        var collection = db.collection ('contacts');
        var ObjectId = "ObjectId(\""+req.params.id+"\")";
        console.log("ObjectId - ", ObjectId);
        collection.deleteOne({"_id": ObjectId}, function(err, result) {
          if(err){
            console.log("Ошибка при удалении данных из БД", err);
            res.status(500);
          } else {
            console.log(result);
            res.status(200);
            res.end();
          };

        });

      } //ends else
      db.close();
  });




});
//==========================restAPI=======================================


app.use("/api/v1", restAPI);

app.listen(server_port)
