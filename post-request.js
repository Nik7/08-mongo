"uses strict";

var request = require('request');

var values = 
  {
    "name": "Ivan",
    "second_name": "Volkov",
    "phone_number": "79153826483"
  };
request({
  method: 'POST',
  url: 'http://localhost:3000/api/v1/contacts/',
  body: values,
  json: true,
  headers: {
    'User-Agent': 'request'
  }
}, (err, res, body) => {
  console.log(res.statusCode);
  console.log(res.body);
});